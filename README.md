# LEDA

Summary:
An open-source toolbox that offers metrics for brain activity measurements based on source localized EEG in high spatiotemporal resolution. Preliminary study involves brain activity analysis of 18 human subjects in response to paired auditory click paradigm. Biomarkers were characterized that significantly distinguished schizophrenia patients from healthy controls. For further details and usage, please check the publication mentioned below.

Please cite:
Gupta, Deepa, Ann Summerfelt, Jennifer Luzhansky, Daniel Li, Elliot Hong, and Fow-Sen Choa. "LEDA-Localized-EEG Dynamics Analyzer: a MATLAB-Based Innovative Toolbox for Analysis of EEG Source Dynamics." Journal of Signal Processing Systems 93, no. 8 (2021): 951-964.
